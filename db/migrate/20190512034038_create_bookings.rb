class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :customer_id
      t.integer :location_id
      t.integer :staff_id
      t.string :status, default: "pending"
      t.datetime :start_time

      t.timestamps
    end
  end
end
