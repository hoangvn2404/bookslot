# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
client = Client.create(name: "IronCap Barbershop", industry: "Beauty Salon", subdomain: "ironcap")
client.switch!
User.create(email: 'admin@ironcap.com', password: 'abcd1234')
location1 = Location.find_or_create_by(name: "Cơ sở 1", address: "27 Cát Linh")
location2 = Location.find_or_create_by(name: "Cơ sở 2", address: "44 Hàng Cháo")
Service.find_or_create_by(name: "Cắt tóc", price: 90000, duration: 30)
Service.find_or_create_by(name: "Nhuộm", price: 120000, duration: 60)
Service.find_or_create_by(name: "Uốn", price: 45000, duration: 45)
location1.staffs.find_or_create_by(name: "Hoàng", gender: "Nam", phone: '0943838106')
location1.staffs.find_or_create_by(name: "Tú", gender: "Nam", phone: '0986315555')
location2.staffs.find_or_create_by(name: "Huy", gender: "Nam", phone: '0944412402')
