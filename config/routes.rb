Rails.application.routes.draw do


  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }

  devise_scope :user do
    authenticated :user do
      # root to: 'students#home', constraints: ->(request) { get_user_class_from(request) == 'Student' }
      # root to: 'application_forms#index', constraints: ->(request) { get_user_class_from(request) == 'Staff' }
      # root to: 'students#home', constraints: ->(request) { request.env['warden'].user.class.name == 'Student' }
      # root to: 'staffs#homepage', constraints: ->(request) { request.env['warden'].user.class.name == 'Staff' }
      # root to: 'staffs#homepage', constraints: ->(request) { get_user_class_from(request) == 'Staff' }
      root 'admin#dashboard'
    end
    unauthenticated do
      root 'pages#home'
    end
  end

  get '/admin' => 'admin#dashboard'
  scope "/admin", module: 'admin', as: :admin do
    resources :staffs
    resources :clients
    resources :services
    resources :locations
    resources :bookings
    resources :customers
  end

  scope '/api', module: 'api', as: :api do
    resources :staffs, only: [:show]
    resources :bookings, only: [:create]
  end



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
