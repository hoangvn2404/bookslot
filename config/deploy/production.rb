role :app, %w{deploy@68.183.235.152}
role :web, %w{deploy@68.183.235.152}
role :db,  %w{deploy@68.183.235.152}

set :ssh_options, {
  forward_agent: false,
  auth_methods: %w(password),
  password: 'dantri.com2',
  user: 'deploy',
}

set :application, 'henlich'
set :repo_url, 'git@bitbucket.org:hoangvn2404/henlich.git'

set :branch, 'master'
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/deploy/henlich'
