# == Schema Information
#
# Table name: booking_service_connects
#
#  id         :bigint           not null, primary key
#  booking_id :integer
#  service_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BookingServiceConnect < ApplicationRecord
  belongs_to :service
  belongs_to :booking
end
