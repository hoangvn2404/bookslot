import React from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'

export default (props) =>
  <FullCalendar
    defaultView="dayGridMonth"
    header={{
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    }}
    plugins={[ dayGridPlugin, timeGridPlugin ]}
    events={[
    { title: 'event 1', date: '2019-05-13' },
    { title: 'event 2', date: '2019-05-13' }
    ]}
  />
