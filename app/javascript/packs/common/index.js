const requireModule = require.context('.', true, /\.js$/)
const modules = {}

requireModule
  .keys()
  .filter(fileName => fileName !== './index.js')
  .forEach(fileName => modules[fileName] = requireModule(fileName).default)

export default () => Object.values(modules).forEach(f => f())
