class DatepickerInput < SimpleForm::Inputs::StringInput
  def input_html_options
    value = @builder.object[attribute_name].try(:strftime, '%d/%m/%Y')
    super.merge(value: value)
  end

  def input_html_classes
    super.push('daterange')
  end
end
