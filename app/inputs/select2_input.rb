class Select2Input < SimpleForm::Inputs::CollectionSelectInput
  def input_html_classes
    super.push('select2enable')
  end
end
