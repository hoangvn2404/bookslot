class Api::BookingsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    customer = Customer.find_or_create_by(phone: params[:customer][:phone])
    customer.update(name: params[:customer][:name])
    booking_params[:customer_id] = customer.id
    @booking = Booking.new(booking_params)
    if @booking.save
      render json: true
    else
      render json: { errors: @document.errors, status: :unprocessable_entity }
    end
  end

  private
    def booking_params
      params.require(:booking).permit!
    end
end
