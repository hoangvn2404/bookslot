class Admin::StaffsController < AdminController
  before_action :set_staff, only: [:show, :edit, :update, :destroy]

  def index
    @staffs = Staff.all
  end

  def show
  end

  def new
    @staff = Staff.new
  end

  def edit
  end

  def create
    @staff = Staff.new(staff_params)

    if @staff.save
      redirect_to admin_staffs_path, notice: 'Staff was successfully created.'
    else
      render :new
    end
  end

  def update
    if @staff.update(staff_params)
      redirect_to admin_staffs_path, notice: 'Staff was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @staff.destroy
    redirect_to admin_staffs_url, notice: 'Staff was successfully destroyed.'
  end

  private
    def set_staff
      @staff = Staff.find(params[:id])
    end

    def staff_params
      params.require(:staff).permit!
    end
end
